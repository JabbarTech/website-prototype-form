<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
.error {
  color: red;
    font-weight: bold;
}
</style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<title>Register</title>
  </head>
  <body>
  <?php
include 'form.php';
?>

   <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Grab a Boat</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="index.php">Home</a></li>
      <li class="active"><a href="register.php">Register</a></li>
      <li><a href="#">Animations</a></li>
    </ul>
  </div>
</nav>
<div class="container">
<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
<div class="form-group">
        <label for="username" class="control-label">Username</label>
        <input type="text" class="form-control" name="username" minlength="6" maxlength="50" value="<?php echo htmlspecialchars($username) ?>" required>
        <span class="error"><?php echo $errors['email']; ?></span>
    </div>  
     <div class="form-group">
    <label for="pwd">Password:</label>
    <input name="password" type="password" class="form-control" minlength="8" maxlength="50" value="<?php echo htmlspecialchars($password);?>"required >
     <span class="error"><?php echo $errors['password'];?></span>
  </div>
  <div class="form-group">
    <label for="pwd"> Repeat Password:</label>
    <input name="confirm_password" type="password" class="form-control" minlength="8" maxlength="50"required >
     <span class="error"><?php echo $errors['confirm_password'];?></span>
  </div>

    <div class="form-group"> 
        <label for="firstname" class="control-label">First Name</label>
        <input type="text" class="form-control"  name="firstname" maxlength="50" required>
        <span class="error"><?php echo $errors['firstname'];?></span>
    </div>
     <div class="form-group"> 
        <label for="lastname" class="control-label">Last Name</label>
        <input type="text" class="form-control"  name="lastname" maxlength="50" required>
         <span class="error"><?php echo $errors['lastname'];?></span>
    </div>    
      <div class="form-group">
    <label for="email">Email address:</label>
    <input type="email" class="form-control"  name="email" required>
     <span class="error"><?php echo $errors['email'];?></span>  </div>
 

    <div class="form-group">
        <label for="street1_id" class="control-label">Street Address 1</label>
        <input type="text" class="form-control" id="street1_id" name="street1" placeholder="Street address, P.O. box, company name, c/o" required >
         <span class="error"><?php echo $errors['street1'];?></span>
      </div>                  
                            
    <div class="form-group">
        <label for="street2_id" class="control-label">Street Address 2</label>
        <input type="text" class="form-control"  name="street2" placeholder="Apartment, suite, unit, building, floor, etc.">
      <span class="error"><?php echo $errors['street2'];?></span>
      </div>  

    <div class="form-group">
        <label for="city_id" class="control-label">City</label>
        <input type="text" class="form-control" name="city" placeholder="Smallville" required>
         <span class="error"><?php echo $errors['city'];?></span>
      </div>                                  
                            
    <div class="form-group"> <!-- State Button -->
        <label for="state_id" class="control-label">State</label>
         <span class="error"><?php echo $errors['state'];?></span>
        <select class="form-control" name="states" required>
            <option value="AL">Alabama</option>
            <option value="AK">Alaska</option>
            <option value="AZ">Arizona</option>
            <option value="AR">Arkansas</option>
            <option value="CA">California</option>
            <option value="CO">Colorado</option>
            <option value="CT">Connecticut</option>
            <option value="DE">Delaware</option>
            <option value="DC">District Of Columbia</option>
            <option value="FL">Florida</option>
            <option value="GA">Georgia</option>
            <option value="HI">Hawaii</option>
            <option value="ID">Idaho</option>
            <option value="IL">Illinois</option>
            <option value="IN">Indiana</option>
            <option value="IA">Iowa</option>
            <option value="KS">Kansas</option>
            <option value="KY">Kentucky</option>
            <option value="LA">Louisiana</option>
            <option value="ME">Maine</option>
            <option value="MD">Maryland</option>
            <option value="MA">Massachusetts</option>
            <option value="MI">Michigan</option>
            <option value="MN">Minnesota</option>
            <option value="MS">Mississippi</option>
            <option value="MO">Missouri</option>
            <option value="MT">Montana</option>
            <option value="NE">Nebraska</option>
            <option value="NV">Nevada</option>
            <option value="NH">New Hampshire</option>
            <option value="NJ">New Jersey</option>
            <option value="NM">New Mexico</option>
            <option value="NY">New York</option>
            <option value="NC">North Carolina</option>
            <option value="ND">North Dakota</option>
            <option value="OH">Ohio</option>
            <option value="OK">Oklahoma</option>
            <option value="OR">Oregon</option>
            <option value="PA">Pennsylvania</option>
            <option value="RI">Rhode Island</option>
            <option value="SC">South Carolina</option>
            <option value="SD">South Dakota</option>
            <option value="TN">Tennessee</option>
            <option value="TX">Texas</option>
            <option value="UT">Utah</option>
            <option value="VT">Vermont</option>
            <option value="VA">Virginia</option>
            <option value="WA">Washington</option>
            <option value="WV">West Virginia</option>
            <option value="WI">Wisconsin</option>
            <option value="WY">Wyoming</option>
        </select>                   
    </div>
    
    <div class="form-group"> <!-- Zip Code-->
        <label for="zip_id" class="control-label">Zip Code</label>
        <input type="text" class="form-control" name="zip" placeholder="#####"maxlength="5" required>
        <span class="error"><?php echo $errors['zip'];?></span>
      </div>      

     <div class="form-group">
     <label for="phone">Phone Number:</label><br/>
        <input id="phone" type="text" name="phone" maxlength="12"/>
        <span class="error"><?php echo $errors['phone'];?></span>
</div>


     <label><strong>Marital Status:</strong></label><br>
<div name= "marital" class="form-check form-check-inline">
  <input type="radio" class="form-check-input" >
  <label class="form-check-label" for="materialInline1">Married</label>


<!-- Material inline 2 -->
<div class="form-check form-check-inline">
  <input type="radio" class="form-check-input"  name="inlineMaterialRadiosExample">
  <label class="form-check-label" for="materialInline2">Single</label>
</div>

<!-- Material inline 3 -->
<div class="form-check form-check-inline">
  <input type="radio" class="form-check-input" name="inlineMaterialRadiosExample">
  <label class="form-check-label" for="materialInline3">Divorced</label>
</div>
<div class="form-check form-check-inline">
  <input type="radio" class="form-check-input" name="inlineMaterialRadiosExample">
  <label class="form-check-label" for="materialInline3">Widow</label>
</div>
 <span class="error"><?php echo $errors['marital'];?></span>
</div>
<label>Gender</label>
<div class="form-check-inline">
  <label class="form-check-label">
    <input type="checkbox" class="form-check-input" value="">Male
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input type="checkbox" class="form-check-input" value="">Female
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input type="checkbox" class="form-check-input" value="">Other

     <span class="error"><?php echo $errors['gender'];?></span>
  </label>
  <br />

  <label for="birthday">Birthday:</label>
                <input type="date" id="birthday" name="birthday" required/>

</div> 
 <input class="btn btn-primary" name="submit" type="submit" value="Submit">
      <input class="btn btn-warning" type="reset" value="Reset">
    
</form>         
 <form id="theConfirmationForm" name="theConfirmationForm"
                      method="POST" action="confirm.php">
                    <?php
                    foreach ($_POST as $key => $value) {
                        ?>
                        <input name="<?php echo $key; ?>"
                               value="<?php echo $value; ?>"
                               type="hidden"/>
                        <?php
                    }
                    ?>

    </div>
  
  </body>
</html>