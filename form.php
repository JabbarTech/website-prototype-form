<?php

$errors = array('username' => '', 'firstname' => '', 'gender' => '', 'lastname' => '','street1' => '','street2' => '','password' => '','confirm_password' => '','marital' => '','zip' => '','city' => '','state' => '','email' => '','gender' => '','phone' => '');

$username = $firstname
= $lastname = $email
= $street1 = $street2
= $password = $confirm_password
= $marital = $zip = $city = $state = $gender = $date =  $phone  = '';


if  (isset($_POST['submit'])) {

    if(empty($POST['username'])) {
        $errors['username'] = 'Username required';
    }else {
        $username = $_POST['username'];

        if (!preg_match("/^[a-zA-Z0-9]+$/", $username)) {
            $errors['username'] = "Only letters, numbers, and white space allowed";    
        }

    }
    if(empty($POST['password'])) {
        $errors['password']  = 'Password is required <br />';
    }else {
        $password = $_POST['password'];
        if (preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,50}$/", $password)){
            $errors['password'] = "Password must be between 8 and 50 characters, 
            have a capital letter, lowercase letter, special character, and
            a number";
        }
    }
    if(empty($POST['confirm_password'])) {
        $errors['confirm_password']  = 'Retype_Password is required <br />';
    }else {
        $confirm_password = $_POST['confirm_password'];
        if($password !== $confirm_password) {
            $confirm_password = "Both passwords must match";
        }
    }

    if (empty($_POST['firstname'])) {
        $errors['firstname'] = "Firstname is required";
    } else {
        if (!preg_match("/^[a-zA-Z]+$/", $firstname)) {
            $lastnameError = "Only letters and white space allowed";
        }
    }
    if (empty($_POST['lastname'])) {
        $errors['lastname'] = "LastName is required";
    } else {
        if (!preg_match("/^[a-zA-Z]+$/", $firstname)) {
            $lastnameError = "Only letters and white space allowed";
        }
    }

    if(empty($_POST['email'])){
        $errors['email'] = 'Email is required';
    } else{
        $email = $_POST['email'];
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $errors['email'] = 'Email must be a valid email address';
        }

    }


    if (empty($_POST['zip'])) {
        $errors['zip']  = "A zip code is required";
        
    } else {
        if (!preg_match("/^\d{5}-\d{4}$/", $zip) && (!preg_match("/^\d{5}$/", $zip))) {
            $errors['zip']  = "Make sure your zip code is in a '12345' or '12345-6789' format!";
        
        }
    }

    if (empty($_POST['street1'])){
        $errors['street1']= "An address is required";
    } else {
        if (!preg_match("/^[a-zA-Z0-9 ]+$/", $street1)) {
            $errors['street1'] = "Only letters, numbers, and white space allowed";
           
        }
    }
    if (!isset($_POST["gender"])) {
        $error['gender'] = "Gender is required";

    }

    if  (empty($_POST['phone'])) {
         $error['phone'] = "A phone number is required";
    } else {
        // check if name only contains letters and whitespace
        if (!preg_match("/^\d{3}-\d{3}-\d{4}$/", $phone)) {
            $error['phone']= "Need '123-456-7890' format!";
        }
    }






    if (empty($_POST['street2'])){
        $errors['street2']= "address2 is required";
    } else {
        if (!preg_match("/^[a-zA-Z0-9 ]+$/", $street1)) {
            $errors['street2'] = "Only letters, numbers, and white space allowed";
           
        }
    }
    if(array_filter($errors)){
            //echo 'errors in form';
        } else {
            //echo 'form is valid';
            header('Location: confirm.php');
        }
}

?>